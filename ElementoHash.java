import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


public class ElementoHash {
	
	private long cepi;
	private long numReg;
	private long proximo;
	
	public void le(DataInput din) throws IOException
	{
		this.cepi = din.readLong();
		this.numReg = din.readLong();
		this.proximo = din.readLong(); 
	}

	public void escreve(DataOutput dout) throws IOException
	{
		dout.writeLong(this.cepi);
		dout.writeLong(this.numReg);
		dout.writeLong(this.proximo);
	}

	public long getCepi() {
		return cepi;
	}

	public void setCepi(long cepi) {
		this.cepi = cepi;
	}

	public long getNumReg() {
		return numReg;
	}

	public void setNumReg(long numReg) {
		this.numReg = numReg;
	}

	public long getProximo() {
		return proximo;
	}

	public void setProximo(long proximo) {
		this.proximo = proximo;
	}
	
	

}
