
import java.io.RandomAccessFile;
import java.util.Scanner;


public class MainHash {

	public static void main(String[] args) throws Exception {
		RandomAccessFile indice = new RandomAccessFile("indice.dat", "rw");
		RandomAccessFile f = new RandomAccessFile("cep.dat", "r");



		long tamHash = 699307; // tamanho inicial do arquivo de indice e divisor 
		
		ElementoHash h = new ElementoHash();
		

		h.setCepi(-1);
		h.setNumReg(-1);
		h.setProximo(-1);
		
        // grava no arquivo de indice os campos acima tamHash vezes
		
		for (int i=0; i<=tamHash; i++){
			
			h.escreve(indice);
		}	
			
	
		indice.seek(0);
        f.seek(0);
			
		
		Endereco r = new Endereco();

		long hash; // guarda o resto da divisisao
		long regF; // guarda o registro antes de ler o arquivo cep
        long regP; // guarda o registro anterior de prox
        
        // monta o arquivo de indice 
        
        long colisao = 0;
		
		while( f.getFilePointer() < f.length() ) // para Detectar EOF
		
		{
			regF = f.getFilePointer();
			
			r.leEndereco(f);
			System.out.println("Lendo CEP: "+r.getCep()); // apenas para ter certeza que o programa n�o ta parado
			hash = Long.valueOf(r.getCep()).longValue()% tamHash;
			indice.seek((hash)*24);

			h.le(indice);

			if (h.getCepi() < 0){	
				h.setCepi(Long.parseLong( r.getCep()));
				h.setNumReg(regF);
				h.setProximo(-1);
				indice.seek((hash)*24);
				h.escreve(indice); 
				
				
			}else  {
				
				// colisao - atualiza o endere�o atual com o proximo = tamarquivo

				colisao++;
				regP = h.getProximo();
				h.setProximo(indice.length());
				indice.seek((hash)*24);
				h.escreve(indice); 
				
				// grava o registro que dever� ser consultado quando houver colisao
				
				h.setCepi(Long.parseLong( r.getCep()));
				h.setNumReg(regF);
				h.setProximo(regP);
				indice.seek(indice.length());
				h.escreve(indice);
								
			}
	
		} 
		System.out.println("**** TOTAL DE COLIS�ES "+colisao);	
		
		indice.seek(0);
        f.seek(0);
        
        // este trecho foi usado s� para ler o arquivo de indice, durante os teste.
        
        
/*		while( indice.getFilePointer() < indice.length() ) // para Detectar EOF
		{

		
			h.le(indice);
			System.out.println("lendo indice ....."+indice.getFilePointer()+" "+h.getCepi()+" "+h.getNumReg()+" "+h.getProximo() );
		} */

        
		// Busca com Hash
        
        Scanner scanner = new Scanner(System.in);

		long ccp = 9l;

      while (ccp>0){		
			System.out.println("Digite o CEP:");
			ccp = scanner.nextLong();
		
			if (ccp < 1){
				System.out.println("***** FIM DAS BUSCAS *****");
				break;
			}
			
			hash = (ccp % tamHash);
			indice.seek((hash)*24);
			h.le(indice);
			while((h.getCepi() != ccp) && (h.getProximo() != -1)){
				indice.seek(h.getProximo());
				h.le(indice);
			}

			if (h.getCepi() == ccp){
				f.seek(h.getNumReg());
				r.leEndereco(f);
				System.out.println(h.getCepi()+" "+r.getCep()+" "+r.getLogradouro());
			}else{	
				System.out.println("Cep n�o encontrado");	
			}
		
		}		
			


		
		
		
		indice.close();
		f.close();

	}
}
